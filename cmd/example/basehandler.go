package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"

	"github.com/gorilla/sessions"
	"gitlab.com/kendellfab/fazer"
	"gitlab.com/kendellfab/lms/actors"
)

type BaseHandler struct {
	*fazer.Fazer
	sessions.Store
	AM            actors.Manager
	JwtSigningKey string
}

func (bh *BaseHandler) PageAuth(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		sess, err := bh.Get(r, authorSession)
		if err != nil {
			log.Println("error loading session", err)
			bh.SetErrorFlash(w, r, "error loading session")
			bh.Redirect(w, r, "/login")
			return
		}

		authorID, ok := sess.Values["authorID"].(string)
		if !ok {
			log.Println("user not found")
			bh.SetErrorFlash(w, r, "must be authenticated")
			bh.Redirect(w, r, "/login")
			return
		}

		author, err := bh.AM.GetAuthor(authorID)
		if err != nil {
			log.Println("error loading user", err)
			bh.SetErrorFlash(w, r, "must be authenticated")
			bh.Redirect(w, r, "/login")
			return
		}

		actors.SetAuthor(author, r)
		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}

func (bh *BaseHandler) ApiAuth(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		auth := r.Header.Get("Authorization")
		tokenString := strings.TrimPrefix(auth, "Bearer ")

		token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("unexpected method: %v", token.Header["alg"])
			}
			return []byte(bh.JwtSigningKey), nil
		})

		if err != nil {
			http.Error(w, "jwt is invalid", http.StatusUnauthorized)
			return
		}

		_, ok := token.Claims.(jwt.MapClaims)
		if !ok || !token.Valid {
			http.Error(w, "jwt is invalid", http.StatusUnauthorized)
			return
		}

		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}
