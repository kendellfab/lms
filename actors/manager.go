package actors

import (
	"errors"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

var ErrAdminExists error = errors.New("admin exists")
var ErrPassword error = errors.New("password match error") // Don't expose this to the end user, just for internal returns

type Manager struct {
	orm *gorm.DB
}

func NewManager(orm *gorm.DB) Manager {
	orm.AutoMigrate(&Author{}, &Student{}, &ResetClaim{})
	return Manager{orm: orm}
}

func (m Manager) NewAuthorAdmin(name, email, password string) (Author, error) {
	var check []Author
	m.orm.Where("role = ?", int(AuthorRoleAdmin)).Find(&check)
	if len(check) > 0 {
		return Author{}, ErrAdminExists
	}

	return m.newAuthorWithRole(name, email, password, AuthorRoleAdmin)
}

func (m Manager) NewAuthorAuthor(name, email, password string) (Author, error) {
	return m.newAuthorWithRole(name, email, password, AuthorRoleAuthor)
}

func (m Manager) newAuthorWithRole(name, email, password string, role AuthorRole) (Author, error) {
	a := Author{Name: name, Email: email}

	if a.Role == AuthorRoleUnknown {
		a.Role = role
	}

	hashed, err := m.hashPassword(password)
	if err != nil {
		return a, err
	}
	a.Password = hashed

	err = m.orm.Save(&a).Error
	a.Password = ""
	return a, err
}

func (m Manager) CompareAuthorCredentials(email, password string) (Author, error) {
	var a Author
	err := m.orm.Where("email = ?", email).First(&a).Error
	if err != nil {
		return Author{}, err
	}

	if !m.isPasswordMatch(password, a.Password) {
		return Author{}, ErrPassword
	}

	a.Password = ""
	return a, nil
}

func (m Manager) GetAuthor(authorID string) (Author, error) {
	var author Author
	err := m.orm.Where(&Author{ID: authorID}).First(&author).Error
	if err == nil {
		author.Password = ""
	}
	return author, err
}

func (m Manager) NewStudent(name, email, password string) (Student, error) {
	s := Student{Name: name, Email: email}

	hashed, err := m.hashPassword(password)
	if err != nil {
		return s, err
	}
	s.Password = hashed

	err = m.orm.Save(&s).Error
	return s, err
}

func (m Manager) CompareStudentCredentials(email, password string) (Student, error) {
	var s Student
	err := m.orm.Where("email = ?", email).First(&s).Error
	if err != nil {
		return Student{}, err
	}

	if !m.isPasswordMatch(password, s.Password) {
		return Student{}, ErrPassword
	}

	// Sanitize the password hash so that it isn't inadvertently leaked
	s.Password = ""
	return s, nil
}

func (m Manager) hashPassword(pass string) (string, error) {
	res, err := bcrypt.GenerateFromPassword([]byte(pass), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(res), err
}

func (m Manager) isPasswordMatch(input, stored string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(stored), []byte(input))
	return err == nil
}
