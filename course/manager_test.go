package course

import (
	"bytes"
	"encoding/json"
	"testing"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func TestAddCourse(t *testing.T) {
	db, err := gorm.Open("sqlite3", ":memory:")
	if err != nil {
		t.Error("error opening database", err)
	}

	input := Course{Title: "Test Course", Description: "The test course"}
	bts, err := json.Marshal(input)
	if err != nil {
		t.Errorf("Error marshaling json of input type: %v", err)
	}

	buf := bytes.NewBuffer(bts)

	manager := NewManager(db)

	course, err := manager.NewCourse(buf)
	if err != nil {
		t.Errorf("Error creating new course: %v", err)
	}

	if course.ID == "" {
		t.Errorf("Error with saving course.  Id is empty should be")
	}

	if course.Title != input.Title {
		t.Errorf("Course titles do not match.  Expected %v Got %v", input.Title, course.Title)
	}

	if course.Slug != "test-course" {
		t.Errorf("Course slug not set.  Expected test-course Got %v", course.Slug)
	}
}

func TestAddLessonToCourse(t *testing.T) {
	db, err := gorm.Open("sqlite3", ":memory:")
	if err != nil {
		t.Error("error opening database", err)
	}

	input := Course{Title: "Test Course", Description: "The test course"}
	bts, err := json.Marshal(input)
	if err != nil {
		t.Errorf("Error marshaling json of input type: %v", err)
	}

	buf := bytes.NewBuffer(bts)

	manager := NewManager(db)

	course, err := manager.NewCourse(buf)
	if err != nil {
		t.Errorf("Error creating new course: %v", err)
	}

	lessonInput := Lesson{Title: "First Lesson"}
	lessonBts, err := json.Marshal(lessonInput)
	if err != nil {
		t.Errorf("Error getting lesson bytes. %v", err)
	}

	lessonBuf := bytes.NewBuffer(lessonBts)
	lesson, err := manager.NewLesson(course.ID, lessonBuf)
	if err != nil {
		t.Errorf("Error saving lesson.  %v", err)
	}

	if lesson.ID == "" {
		t.Errorf("Error saving lesson, ID is empty")
	}

	if lesson.Slug != "first-lesson" {
		t.Errorf("Slug not generated correctly.  Expecte first-lesson Got %v", lesson.Slug)
	}

}

func TestAddCategory(t *testing.T) {
	db, err := gorm.Open("sqlite3", ":memory:")
	if err != nil {
		t.Error("error opening database", err)
	}

	catInput := Category{Title: "Demo", Description: "A demo category"}
	catBytes, err := json.Marshal(catInput)
	if err != nil {
		t.Error("error marshaling json of category", err)
	}

	catBuf := bytes.NewBuffer(catBytes)

	manager := NewManager(db)
	resultCat, err := manager.NewCategory(catBuf)
	if err != nil {
		t.Error("error creating new category", err)
	}

	if resultCat.ID == "" {
		t.Error("Course ID should not be empty.")
	}

	input := Course{Title: "Test Course", Description: "The test course", Active: true}
	bts, err := json.Marshal(input)
	if err != nil {
		t.Errorf("Error marshaling json of input type: %v", err)
	}

	buf := bytes.NewBuffer(bts)

	course, err := manager.NewCourse(buf)
	if err != nil {
		t.Errorf("Error creating new course: %v", err)
	}

	err = manager.AddCourseToCategory(resultCat.ID, course.ID)
	if err != nil {
		t.Error("error adding course to category", err)
	}

	cat, err := manager.GetCategoryWithCourses(resultCat.Slug, 100, 0)
	if err != nil {
		t.Error("error loading saved category with courses", err)
	}

	if len(cat.Courses) == 0 {
		t.Errorf("Expected courses of length at least 1 got 0")
	}
}
