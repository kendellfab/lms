package main

import (
	"log"
	"net/http"

	"gitlab.com/kendellfab/lms/course"

	"github.com/go-chi/chi"

	"github.com/dgrijalva/jwt-go"

	"github.com/gorilla/sessions"
	"gitlab.com/kendellfab/lms/actors"
)

type FrontHandler struct {
	*BaseHandler
	actors.Manager
	CourseManager course.Manager
}

func (fh *FrontHandler) GetIndex(w http.ResponseWriter, r *http.Request) {
	fh.RenderTemplate(w, r, nil, "front.index")
}

func (fh *FrontHandler) GetLogin(w http.ResponseWriter, r *http.Request) {
	fh.RenderTemplate(w, r, nil, "front.login")
}

func (fh *FrontHandler) PostLogin(w http.ResponseWriter, r *http.Request) {
	email := r.PostFormValue("email")
	pass := r.PostFormValue("password")

	author, err := fh.CompareAuthorCredentials(email, pass)
	if err != nil {
		log.Println("error logging in", err)
		fh.SetErrorFlash(w, r, "error logging in")
		fh.Redirect(w, r, "/login")
		return
	}

	session, err := fh.Get(r, authorSession)
	if err != nil {
		log.Println("error getting session", err)
		fh.SetErrorFlash(w, r, "could not load session")
		fh.Redirect(w, r, "/login")
		return
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"authorId":   author.ID,
		"authorRole": author.Role,
	})

	tokenString, err := token.SignedString([]byte(fh.JwtSigningKey))
	if err != nil {
		log.Println("error signing token", err)
		fh.SetErrorFlash(w, r, "error signing in")
		fh.Redirect(w, r, "/login")
		return
	}

	session.Values["authorID"] = author.ID
	session.Values["authorRole"] = author.Role
	session.Values["jwt"] = tokenString
	if err := session.Save(r, w); err != nil {
		log.Println("error saving session", err)
		fh.SetErrorFlash(w, r, "error saving session")
		fh.Redirect(w, r, "/login")
		return
	}

	fh.Redirect(w, r, "/admin/dashboard")
}

func (fh *FrontHandler) GetLogout(w http.ResponseWriter, r *http.Request) {
	session, err := fh.Get(r, "author-session")
	if err != nil {
		log.Println("error loading session", err)
		fh.SetErrorFlash(w, r, "error loading session")
		fh.Redirect(w, r, "/login")
		return
	}

	session.Options.MaxAge = -1
	sessions.Save(r, w)
	fh.Redirect(w, r, "/")
}

func (fh *FrontHandler) GetSetupAuthor(w http.ResponseWriter, r *http.Request) {
	fh.RenderTemplate(w, r, nil, "front.setupauthor")
}

func (fh *FrontHandler) PostSetupAuthor(w http.ResponseWriter, r *http.Request) {
	name := r.PostFormValue("name")
	email := r.PostFormValue("email")
	pass := r.PostFormValue("password")

	_, err := fh.NewAuthorAdmin(name, email, pass)
	if err != nil {
		fh.SetErrorFlash(w, r, "error creating author")
		fh.Redirect(w, r, "/setup-author")
		return
	}

	fh.SetSuccessFlash(w, r, "author created")
	fh.Redirect(w, r, "/login")
}

func (fh *FrontHandler) GetRegisterStudent(w http.ResponseWriter, r *http.Request) {
	fh.RenderTemplate(w, r, nil, "front.registerstudent")
}

func (fh *FrontHandler) GetCourseList(w http.ResponseWriter, r *http.Request) {
	courses, err := fh.CourseManager.GetActiveCourses(100, 0)
	fh.RenderTemplate(w, r, map[string]interface{}{"courses": courses, "err": err}, "front.courselist")
}

func (fh *FrontHandler) GetCoursePlayer(w http.ResponseWriter, r *http.Request) {
	slug := chi.URLParam(r, "slug")
	c, err := fh.CourseManager.GetCourseBySlug(slug)
	fh.RenderTemplate(w, r, map[string]interface{}{"slug": slug, "course": c, "err": err}, "front.courseplayer")
}
