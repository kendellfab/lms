package main

import (
	"net/http"

	"github.com/go-chi/chi"
)

type AdminHandler struct {
	*BaseHandler
}

func (ah AdminHandler) GetDashboard(w http.ResponseWriter, r *http.Request) {
	ah.RenderTemplate(w, r, nil, "admin.dashboard")
}

func (ah AdminHandler) GetCategories(w http.ResponseWriter, r *http.Request) {
	ah.RenderTemplate(w, r, nil, "admin.categories")
}

func (ah AdminHandler) EditCategory(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	ah.RenderTemplate(w, r, map[string]interface{}{"Id": id}, "admin.editcategory")
}

func (ah AdminHandler) GetCourses(w http.ResponseWriter, r *http.Request) {
	ah.RenderTemplate(w, r, nil, "admin.courses")
}

func (ah AdminHandler) EditCourse(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	ah.RenderTemplate(w, r, map[string]interface{}{"Id": id}, "admin.editcourse")
}
