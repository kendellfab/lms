package actors

import "encoding/json"

type AuthorRole int

const (
	AuthorRoleUnknown AuthorRole = 0
	AuthorRoleAuthor  AuthorRole = 5
	AuthorRoleAdmin   AuthorRole = 10
)

const (
	ARUnknown = "Unknown"
	ARAuthor  = "Author"
	ARAdmin   = "Admin"
)

func AuthorRoleFromString(ar string) AuthorRole {
	switch ar {
	case ARAuthor:
		return AuthorRoleAuthor
	case ARAdmin:
		return AuthorRoleAdmin
	default:
		return AuthorRoleUnknown
	}
}

func (ar AuthorRole) String() string {
	switch ar {
	case AuthorRoleAuthor:
		return ARAuthor
	case AuthorRoleAdmin:
		return ARAdmin
	default:
		return ARUnknown
	}
}

func (ar AuthorRole) MarshalJSON() ([]byte, error) {
	return json.Marshal(ar.String())
}

func (ar *AuthorRole) UnmarshalJSON(data []byte) error {
	*ar = AuthorRoleFromString(string(data))
	return nil
}
