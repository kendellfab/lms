package course

import (
	"strings"
	"time"

	"github.com/gofrs/uuid"
	"github.com/jinzhu/gorm"
)

// `gorm:"index:addr"`

type Course struct {
	ID          string     `gorm:"type:varchar(48)" json:"id"`
	Title       string     `json:"title"`
	Slug        string     `gorm:"index:course_slug_idx" json:"slug"`
	Description string     `json:"description"`
	Cover       string     `gorm:"type:varchar(1024)" json:"cover"`
	Lessons     []Lesson   `gorm:"foreignkey:CourseID" json:"lessons"`
	CreatedAt   time.Time  `json:"createdAt"`
	UpdatedAt   time.Time  `json:"updatedAt"`
	DeletedAt   *time.Time `sql:"index" json:",omitempty"`
	CategoryID  string     `json:"categoryId"`
	Active      bool       `json:"active"`
	AuthorID    string     `json:"authorId"`

	OgURL              string `json:"ogurl"`
	OgTitle            string `json:"ogtitle"`
	OgDescription      string `json:"ogdescription"`
	OgImage            string `json:"ogimage"`
	OgImageType        string `json:"ogimagetype"`
	OgImageWidth       string `json:"ogimagewidth"`
	OgImageHeight      string `json:"ogimageheight"`
	FbAppID            string `json:"fbappid"`
	OgType             string `json:"ogtype"`
	OgLocale           string `json:"oglocale"`
	TwitterCard        string `json:"twittercard"`
	TwitterSite        string `json:"twittersite"`
	TwitterTitle       string `json:"twittertitle"`
	TwitterDescription string `json:"twitterdescription"`
	TwitterCreator     string `json:"twittercreator"`
	TwitterImage       string `json:"twitterimage"`
}

// GetTitle returns the page title, intended to be called from the template with a default value
func (c Course) GetTitle(def string) string {
	if c.Title == "" {
		return def
	}
	return c.Title
}

// GetDescription returns page description, intended to be called from the template with a default value
func (c Course) GetDescription(def string) string {
	if c.Description == "" {
		return def
	}
	return c.Description
}

// GetOgUrl returns the open graph url or the canonical url
func (c Course) GetOgUrl(def string) string {
	if c.OgURL != "" {
		return c.OgURL
	}
	return def
}

// GetOgTitle returns the open graph title for the page, or the regular title, or the default value passed in
func (c Course) GetOgTitle(def string) string {
	if c.OgTitle != "" {
		return c.OgTitle
	} else if c.Title != "" {
		return c.Title
	} else {
		return def
	}
}

// GetOgDescription returns the open graph description, the regular description, or the default value passed in
func (c Course) GetOgDescription(def string) string {
	if c.OgDescription != "" {
		return c.OgDescription
	} else if c.Description != "" {
		return c.Description
	} else {
		return def
	}
}

// GetOgImage returns the open graph image or the default value passed in
func (c Course) GetOgImage(def string) string {
	if c.OgImage != "" {
		return c.OgImage
	}
	return def
}

// GetFbAppId returns the fb app id or the default value passed in
func (c Course) GetFbAppId(def string) string {
	if c.FbAppID != "" {
		return c.FbAppID
	}
	return def
}

// GetOgType returns the open graph type or the default website
func (c Course) GetOgType() string {
	if c.OgType != "" {
		return c.OgType
	}
	return "website"
}

// GetOgLocale returns the open graph locale or the default locale
func (c Course) GetOgLocale() string {
	if c.OgLocale != "" {
		return c.OgLocale
	}
	return "en_US"
}

// GetTwitterCard returns the twitter card summary
func (c Course) GetTwitterCard(def string) string {
	if c.TwitterCard != "" {
		return c.TwitterCard
	} else if c.Title != "" {
		return c.Title
	}
	return def
}

// GetTwitterSite returns the twitter site or default.  i.e. @KendellFab
func (c Course) GetTwitterSite(def string) string {
	if c.TwitterSite != "" {
		return c.TwitterSite
	}
	return def
}

// GetTwitterTitle returns the twitter title or the page title or the default
func (c Course) GetTwitterTitle(def string) string {
	if c.TwitterTitle != "" {
		return c.TwitterTitle
	} else if c.Title != "" {
		return c.Title
	}
	return def
}

// GetTwitterDescription returns the twitter description or the page description or the default
func (c Course) GetTwitterDescription(def string) string {
	if c.TwitterDescription != "" {
		return c.TwitterDescription
	} else if c.Description != "" {
		return c.Description
	}
	return def
}

// GetTwitterCreator returns the twitter creator or default
func (c Course) GetTwitterCreator(def string) string {
	if c.TwitterCreator != "" {
		return c.TwitterCreator
	}
	return def
}

// GetTwitterImage returns the twitter image or default
func (c Course) GetTwitterImage(def string) string {
	if c.TwitterImage != "" {
		return c.TwitterImage
	}
	return def
}

func (c *Course) BeforeCreate(scope *gorm.Scope) error {
	id, err := uuid.NewV4()
	if err != nil {
		return nil
	}
	return scope.SetColumn("ID", id.String())
}

func (c *Course) BeforeSave(scope *gorm.Scope) error {
	slug := strings.ToLower(strings.Replace(c.Title, " ", "-", -1))
	c.Slug = slug
	return nil
}
