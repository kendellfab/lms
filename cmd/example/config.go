package main

type config struct {
	Addr          string `default:":3003"`
	Session       string `default:"abc123"`
	JwtSigningKey string `default:"not-secure"`
}
