package course

import (
	"strings"
	"time"

	"github.com/gofrs/uuid"
	"github.com/jinzhu/gorm"
)

type Lesson struct {
	ID          string     `gorm:"type:varchar(48)" json:"id"`
	Title       string     `json:"title"`
	Slug        string     `json:"slug"`
	Description string     `json:"description"`
	Cover       string     `gorm:"type:varchar(1024)" json:"cover"`
	CourseID    string     `json:"courseId"`
	EmbedCode   string     `json:"embedCode"`
	VideoPath   string     `json:"videoPath"` // To be used as a raw playable url, i.e. S3
	VideoID     string     `json:"videoId"`
	VideoURL    string     `json:"videoUrl"` // To be used with say, Viemo, to identify a video to be played
	Duration    string     `json:"duration"`
	Free        bool       `json:"free"`
	Handouts    []Handout  `gorm:"foreignkey:LessonID" json:"handouts"`
	CreatedAt   time.Time  `json:"createdAt"`
	UpdatedAt   time.Time  `json:"updatedAt"`
	DeletedAt   *time.Time `sql:"index" json:",omitempty"`
	Active      bool       `json:"active"`
}

func (l *Lesson) BeforeCreate(scope *gorm.Scope) error {
	id, err := uuid.NewV4()
	if err != nil {
		return nil
	}
	return scope.SetColumn("ID", id.String())
}

func (l *Lesson) BeforeSave(scope *gorm.Scope) error {
	slug := strings.ToLower(strings.Replace(l.Title, " ", "-", -1))
	l.Slug = slug
	return nil
}
