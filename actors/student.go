package actors

import (
	"time"

	"github.com/gofrs/uuid"
	"github.com/jinzhu/gorm"
)

type Student struct {
	ID        string     `gorm:"type:varchar(48)" json:"id"`
	CreatedAt time.Time  `json:"createdAt"`
	UpdatedAt time.Time  `json:"updatedAt"`
	DeletedAt *time.Time `sql:"index" json:",omitempty"`

	Name             string       `json:"name"`
	Email            string       `gorm:"index:student_email_idx" json:"email"`
	Password         string       `json:"-"`
	Avatar           string       `json:"avatar"`
	StripeID         string       `json:"stripeId"`
	Level            StudentLevel `json:"studentLevel"`
	SubscriptionDate *time.Time   `json:"subscriptionDate"`
}

func (s *Student) BeforeCreate(scope *gorm.Scope) error {
	id, err := uuid.NewV4()
	if err != nil {
		return err
	}
	return scope.SetColumn("ID", id.String())
}
