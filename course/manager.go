package course

import (
	"encoding/json"
	"io"

	"github.com/jinzhu/gorm"
)

type Manager struct {
	orm *gorm.DB
}

func NewManager(orm *gorm.DB) Manager {
	orm.AutoMigrate(&Course{}, &Lesson{}, &Handout{}, &Category{})
	return Manager{orm: orm}
}

func (m Manager) GetCategories(limit, offset int) ([]Category, error) {
	var cats []Category
	err := m.orm.Limit(limit).Offset(offset).Find(&cats).Error
	return cats, err
}

func (m Manager) NewCategory(r io.Reader) (Category, error) {
	var cat Category
	err := json.NewDecoder(r).Decode(&cat)
	if err != nil {
		return cat, err
	}

	err = m.orm.Save(&cat).Error
	return cat, err
}

func (m Manager) GetCategoryWithCourses(slug string, limit, offset int) (Category, error) {
	var cat Category
	err := m.orm.Find(&cat, Category{Slug: slug}).Error
	if err != nil {
		return cat, err
	}

	var courses []Course
	err = m.orm.Model(&cat).Limit(limit).Offset(offset).Where(&Course{Active: true}).Related(&courses, "Courses").Error
	cat.Courses = courses
	return cat, err
}

func (m Manager) UpdateCategory(id string, r io.Reader) (Category, error) {
	var incoming Category
	err := json.NewDecoder(r).Decode(&incoming)
	if err != nil {
		return incoming, err
	}

	var persisted Category
	err = m.orm.Find(&persisted, Category{ID: id}).Error
	if err != nil {
		return incoming, err
	}

	persisted.Title = incoming.Title
	persisted.Description = incoming.Description
	persisted.Cover = incoming.Cover

	err = m.orm.Save(&persisted).Error
	return persisted, err
}

func (m Manager) AddCourseToCategory(catID, courseID string) error {
	var cat Category
	err := m.orm.Find(&cat, Category{ID: catID}).Error
	if err != nil {
		return err
	}

	var course Course
	err = m.orm.Find(&course, Course{ID: courseID}).Error
	if err != nil {
		return err
	}

	err = m.orm.Model(&cat).Association("Courses").Append(&course).Error
	return err
}

func (m Manager) GetCourses(limit, offset int) ([]Course, error) {
	var courses []Course
	err := m.orm.Limit(limit).Offset(offset).Find(&courses).Error
	return courses, err
}

func (m Manager) GetActiveCourses(limit, offset int) ([]Course, error) {
	var courses []Course
	err := m.orm.Limit(limit).Offset(offset).Where(&Course{Active: true}).Find(&courses).Error
	return courses, err
}

func (m Manager) GetCourse(id string) (Course, error) {
	var course Course
	err := m.orm.Preload("Lessons").Preload("Lessons.Handouts").Find(&course, Course{ID: id}).Error
	return course, err
}

func (m Manager) GetCourseBySlug(slug string) (Course, error) {
	var course Course
	err := m.orm.Preload("Lessons", &Lesson{Active: true}).Preload("Lessons.Handouts").Find(&course, Course{Slug: slug}).Error
	return course, err
}

func (m Manager) NewCourse(r io.Reader) (Course, error) {
	var c Course
	err := json.NewDecoder(r).Decode(&c)
	if err != nil {
		return Course{}, err
	}

	err = m.orm.Save(&c).Error
	return c, err
}

func (m Manager) UpdateCourse(courseID string, r io.ReadCloser) (Course, error) {
	var incoming Course
	err := json.NewDecoder(r).Decode(&incoming)
	if err != nil {
		return Course{}, err
	}

	var persisted Course
	m.orm.Find(&persisted, Course{ID: courseID})
	persisted.Title = incoming.Title
	persisted.Description = incoming.Description
	persisted.Cover = incoming.Cover
	persisted.CategoryID = incoming.CategoryID
	persisted.Active = incoming.Active

	err = m.orm.Save(&persisted).Error
	return persisted, err
}

func (m Manager) NewLesson(courseID string, r io.Reader) (Lesson, error) {
	var l Lesson
	err := json.NewDecoder(r).Decode(&l)
	if err != nil {
		return Lesson{}, err
	}

	var c Course
	err = m.orm.Find(&c, Course{ID: courseID}).Error
	if err != nil {
		return Lesson{}, err
	}

	err = m.orm.Model(&c).Association("Lessons").Append(&l).Error
	return l, err
}

func (m Manager) GetLesson(id string) (Lesson, error) {
	var l Lesson
	err := m.orm.Find(&l, Lesson{ID: id}).Error
	return l, err
}

func (m Manager) UpdateLesson(lessonID string, r io.ReadCloser) (Lesson, error) {
	var incoming Lesson
	err := json.NewDecoder(r).Decode(&incoming)
	if err != nil {
		return Lesson{}, err
	}

	var persisted Lesson
	m.orm.Find(&persisted, Lesson{ID: lessonID})
	persisted.Title = incoming.Title
	persisted.Description = incoming.Description
	persisted.Cover = incoming.Cover
	persisted.EmbedCode = incoming.EmbedCode
	persisted.VideoPath = incoming.VideoPath
	persisted.VideoID = incoming.VideoID
	persisted.VideoURL = incoming.VideoURL
	persisted.Active = incoming.Active
	persisted.Free = incoming.Free

	err = m.orm.Save(&persisted).Error
	return persisted, err
}

func (m Manager) DeleteLesson(courseID, lessonID string) error {
	var course Course
	err := m.orm.Find(&course, Course{ID: courseID}).Error
	if err != nil {
		return err
	}

	var lesson Lesson
	err = m.orm.Find(&lesson, Lesson{ID: lessonID}).Error
	if err != nil {
		return err
	}

	return m.orm.Model(&course).Association("Lessons").Delete(&lesson).Error
}

func (m Manager) CreateHandout(lessonID string, r io.ReadCloser) (Handout, error) {
	var lesson Lesson
	err := m.orm.Find(&lesson, Lesson{ID: lessonID}).Error
	if err != nil {
		return Handout{}, err
	}

	var handout Handout
	err = json.NewDecoder(r).Decode(&handout)
	if err != nil {
		return Handout{}, err
	}

	err = m.orm.Model(&lesson).Association("Handouts").Append(&handout).Error
	return handout, err
}

func (m Manager) DeleteHandout(lessonID, handoutID string) error {
	var lesson Lesson
	err := m.orm.Find(&lesson, Lesson{ID: lessonID}).Error
	if err != nil {
		return err
	}

	var handout Handout
	err = m.orm.Find(&handout, Handout{ID: handoutID}).Error
	if err != nil {
		return err
	}
	return m.orm.Model(&lesson).Association("Handouts").Delete(&handout).Error
}
