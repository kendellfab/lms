package actors

import (
	"context"
	"net/http"
	"time"

	"github.com/gofrs/uuid"
	"github.com/jinzhu/gorm"
)

type Author struct {
	ID        string     `gorm:"type:varchar(48)" json:"id"`
	CreatedAt time.Time  `json:"createdAt"`
	UpdatedAt time.Time  `json:"updatedAt"`
	DeletedAt *time.Time `sql:"index" json:",omitempty"`

	Name     string     `json:"name"`
	Email    string     `gorm:"index:author_email_idx" json:"email"`
	Password string     `json:"-"`
	Avatar   string     `json:"avatar"`
	Bio      string     `gorm:"type:text" json:"bio"`
	Role     AuthorRole `json:"role"`
}

func (a *Author) BeforeCreate(scope *gorm.Scope) error {
	id, err := uuid.NewV4()
	if err != nil {
		return err
	}
	return scope.SetColumn("ID", id.String())
}

type key int

var authorKey = key(22)

func SetAuthor(author Author, r *http.Request) {
	ctx := r.Context()
	ctx = context.WithValue(ctx, authorKey, author)
	*r = *(r.WithContext(ctx))
}

func AuthorFromContext(r *http.Request) (Author, bool) {
	author, ok := r.Context().Value(authorKey).(Author)
	return author, ok
}
