package actors

import "encoding/json"

type StudentLevel int

const (
	StudentLevelUnknown    = 0
	StudentLevelGuest      = 1
	StudentLevelSubscribed = 5
	StudentLevelYearComp   = 10
	StudentLevelLifeComp   = 25
)

const (
	SLUnknown    = "Unknown"
	SLGuest      = "Guest"
	SLSubscribed = "Subscribed"
	SLYearComp   = "YearComp"
	SLLifeComp   = "LifeComp"
)

func StudentLevelFromString(sl string) StudentLevel {
	switch sl {
	case SLGuest:
		return StudentLevelGuest
	case SLSubscribed:
		return StudentLevelSubscribed
	case SLYearComp:
		return StudentLevelYearComp
	case SLLifeComp:
		return StudentLevelLifeComp
	default:
		return StudentLevelUnknown
	}
}

func (sl StudentLevel) String() string {
	switch sl {
	case StudentLevelGuest:
		return SLGuest
	case StudentLevelSubscribed:
		return SLSubscribed
	case StudentLevelYearComp:
		return SLYearComp
	case StudentLevelLifeComp:
		return SLLifeComp
	default:
		return SLUnknown
	}
}

func (sl StudentLevel) MarshalJSON() ([]byte, error) {
	return json.Marshal(sl.String())
}

func (sl *StudentLevel) UnmarshalJSON(data []byte) error {
	*sl = StudentLevelFromString(string(data))
	return nil
}
