package actors

import (
	"testing"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func TestAddAuthor(t *testing.T) {
	db, err := gorm.Open("sqlite3", ":memory:")
	if err != nil {
		t.Error("error opening database", err)
	}

	manager := NewManager(db)

	author, err := manager.NewAuthorAdmin("John Doe", "john@doe.com", "s3cr3tp@ssword")
	if err != nil {
		t.Error("error saving author", err)
	}

	if author.ID == "" {
		t.Error("author id expected")
	}

	if author.Name != "John Doe" {
		t.Errorf("error name not correct.  Expected %s But got %s", "John Doe", author.Name)
	}

	if author.Password != "" {
		t.Errorf("expected password to be sanitized but got %s instead", author.Password)
	}

	loadedAuthor, err := manager.CompareAuthorCredentials("john@doe.com", "s3cr3tp@ssword")
	if err != nil {
		t.Error("could not load author from database")
	}

	if loadedAuthor.ID != author.ID {
		t.Errorf("error loading author.  Got %s but expected %s", loadedAuthor.ID, author.ID)
	}

	_, err = manager.NewAuthorAdmin("Jane Doe", "jane@doe.com", "p@ssword")
	if err != ErrAdminExists {
		t.Errorf("expected error adding new admin but got %v", err)
	}
}
