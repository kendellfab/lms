package main

import (
	"encoding/gob"
	"flag"
	"log"
	"net/http"
	"strings"

	"gitlab.com/kendellfab/web/upload"

	"github.com/gorilla/sessions"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/kelseyhightower/envconfig"
	"github.com/wader/gormstore"
	"gitlab.com/kendellfab/fazer"
	"gitlab.com/kendellfab/lms/actors"
	"gitlab.com/kendellfab/lms/course"
)

func main() {

	var manifestPath string
	var assetPath string
	var tplPath string
	flag.StringVar(&manifestPath, "m", "cmd/example/static/manifest.json", "Set the path to the manifest file.")
	flag.StringVar(&assetPath, "a", "cmd/example/static/assets", "Set the path to the assets directory")
	flag.StringVar(&tplPath, "t", "cmd/example/static/tpls", "Set the path to the template directory")
	flag.Parse()

	conf, err := initConfig()
	if err != nil {
		log.Fatal("could not load env vars", err)
	}

	db, err := gorm.Open("sqlite3", "example.db")
	if err != nil {
		log.Fatal("Error connecting to database", err)
	}
	defer db.Close()

	mux := chi.NewRouter()
	mux.Use(middleware.Logger)
	mux.Use(middleware.Recoverer)

	manifest := fazer.MustDecodeManifestJson(manifestPath, fazer.AssetsPath(assetPath))

	store := gormstore.New(db, []byte(conf.Session))
	gob.Register(actors.AuthorRoleAdmin)
	f := fazer.NewFazer(store, tplPath, nil, false)
	manifestErr := f.AddManifest(manifest)
	if manifestErr != nil {
		log.Fatal(manifestErr)
	}
	f.RegisterMerge(fazerMerge(store))
	f.RegisterTemplate("messages", "partials/messages.gohtml")

	courseManager := course.NewManager(db)

	actorManager := actors.NewManager(db)

	baseHandler := &BaseHandler{Fazer: f, Store: store, AM: actorManager, JwtSigningKey: conf.JwtSigningKey}
	frontHandler := &FrontHandler{BaseHandler: baseHandler, Manager: actorManager, CourseManager: courseManager}
	adminHandler := &AdminHandler{BaseHandler: baseHandler}

	// region UploadCreation
	categoryUploadStorageDriver := upload.NewFileStorage("cmd/example/uploads/category")
	categoryUploadHandler := upload.NewImageUpload("/uploads/category", categoryUploadStorageDriver)

	courseUploadStorageDriver := upload.NewFileStorage("cmd/example/uploads/course")
	courseUploadHandler := upload.NewImageUpload("/uploads/course", courseUploadStorageDriver)

	lessonUploadStorageDriver := upload.NewFileStorage("cmd/example/uploads/lesson")
	lessonUploadHandler := upload.NewImageUpload("/uploads/lesson", lessonUploadStorageDriver)

	videoUploadStorageDriver := upload.NewFileStorage("cmd/example/uploads/videos")
	videoUploadHandler := upload.NewFileUpload("/uploads/videos", videoUploadStorageDriver)
	// endregion

	courseHandler := course.NewHandler(courseManager, f)
	apiMux := chi.NewRouter()
	apiMux.Use(baseHandler.ApiAuth)
	courseHandler.RegisterRoutes(apiMux)
	mux.Mount("/api", apiMux)

	// region Frontend
	mux.Get("/", frontHandler.GetIndex)
	mux.Get("/login", frontHandler.GetLogin)
	mux.Post("/login", frontHandler.PostLogin)
	mux.Get("/logout", frontHandler.GetLogout)
	mux.Get("/setup-author", frontHandler.GetSetupAuthor)
	mux.Post("/setup-author", frontHandler.PostSetupAuthor)
	mux.Get("/register-student", frontHandler.GetRegisterStudent)
	mux.Get("/course-list", frontHandler.GetCourseList)
	mux.Get("/course-player/{slug}", frontHandler.GetCoursePlayer)
	// endregion

	// region Admin
	adminMux := chi.NewRouter()
	adminMux.Use(baseHandler.PageAuth)
	adminMux.Get("/dashboard", adminHandler.GetDashboard)
	adminMux.Get("/categories", adminHandler.GetCategories)
	adminMux.Get("/categories/{id}", adminHandler.EditCategory)
	adminMux.Get("/courses", adminHandler.GetCourses)
	adminMux.Get("/courses/{id}", adminHandler.EditCourse)
	mux.Mount("/admin", adminMux)
	// endregion

	// region Uploads
	mux.With(baseHandler.ApiAuth).Post("/uploads/category", categoryUploadHandler.DoUpload)
	mux.Get("/uploads/lesson/{year}/{month}/{file}", categoryUploadHandler.HandleGet)

	mux.With(baseHandler.ApiAuth).Post("/uploads/course", courseUploadHandler.DoUpload)
	mux.Get("/uploads/course/{year}/{month}/{file}", courseUploadHandler.HandleGet)

	mux.With(baseHandler.ApiAuth).Post("/uploads/lesson", lessonUploadHandler.DoUpload)
	mux.Get("/uploads/lesson/{year}/{month}/{file}", lessonUploadHandler.HandleGet)

	mux.With(baseHandler.ApiAuth).Post("/uploads/videos", videoUploadHandler.DoUpload)
	mux.Get("/uploads/videos/{year}/{month}/{file}", videoUploadHandler.HandleGet)
	// endregion

	mux.Handle("/*", http.FileServer(http.Dir(assetPath)))

	srv := http.Server{
		Addr:    conf.Addr,
		Handler: mux,
	}

	err = srv.ListenAndServe()
	if err != nil {
		log.Println("Error listening and serving", err)
	}
}

func fazerMerge(store sessions.Store) func(r *http.Request, res map[string]interface{}) {
	return func(r *http.Request, res map[string]interface{}) {
		if strings.HasPrefix(r.URL.Path, "/admin") {
			if author, ok := actors.AuthorFromContext(r); ok {
				res["Author"] = author

				if session, err := store.Get(r, authorSession); err == nil {
					res["Jwt"] = session.Values["jwt"].(string)
				} else {
					log.Println("error loading session", err)
				}
			}
		}
	}
}

func initConfig() (config, error) {
	var c config
	err := envconfig.Process("lms", &c)
	return c, err
}
