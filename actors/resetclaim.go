package actors

import (
	"time"

	"github.com/gofrs/uuid"
	"github.com/jinzhu/gorm"
)

type ResetClaim struct {
	ID        string     `gorm:"type:varchar(48)" json:"id"`
	CreatedAt time.Time  `json:"createdAt"`
	UpdatedAt time.Time  `json:"updatedAt"`
	DeletedAt *time.Time `sql:"index" json:",omitempty"`

	ActorID string    `json:"actorId"`
	Token   string    `json:"token" gorm:"type:varchar(2048);index:reset_claim_token_idx"`
	Expires time.Time `json:"expires"`
}

func (rc *ResetClaim) BeforeCreate(scope *gorm.Scope) error {
	id, err := uuid.NewV4()
	if err != nil {
		return err
	}
	return scope.SetColumn("ID", id.String())
}
