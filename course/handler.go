package course

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"gitlab.com/kendellfab/fazer"
)

// Handler holds the manager and the fazer output renderer
type Handler struct {
	manager Manager
	f       *fazer.Fazer
}

// NewHandler returns a new handler
func NewHandler(m Manager, f *fazer.Fazer) Handler {
	return Handler{manager: m, f: f}
}

// RegisterRoutes takes a mux, should be a sub router with the desired auth middleware attached
func (h Handler) RegisterRoutes(mux *chi.Mux) {
	mux.Get("/lms/category", h.GetCategories)
	mux.Post("/lms/category", h.CreateCategory)
	mux.Get("/lms/category/{slug}", h.GetCategory)
	mux.Put("/lms/category/{id}", h.UpdateCategory)
	mux.Get("/lms/course", h.GetCourses)
	mux.Get("/lms/course/{id}", h.GetCourse)
	mux.Post("/lms/course", h.CreateCourse)
	mux.Put("/lms/course/{id}", h.UpdateCourse)
	mux.Post("/lms/course/{id}/lesson", h.CreateLesson)
	mux.Get("/lms/course/{id}/lesson/{lid}", h.GetLesson)
	mux.Put("/lms/course/{id}/lesson/{lid}", h.UpdateLesson)
	mux.Delete("/lms/course/{id}/lesson/{lid}", h.DeleteLesson)
	mux.Post("/lms/course/{id}/lesson/{lid}/handout", h.CreateHandout)
	mux.Delete("/lms/course/{id}/lesson/{lid}/handout/{hid}", h.DeleteHandout)
}

func (h Handler) GetCategories(w http.ResponseWriter, r *http.Request) {
	limit := h.queryInt(r, "limit", 100)
	page := h.queryInt(r, "page", 1)
	cats, err := h.manager.GetCategories(limit, (page-1)*limit)
	if err != nil {
		log.Println("error getting categories", err)
		http.Error(w, "error loading categories", http.StatusInternalServerError)
		return
	}
	h.f.RenderJson(w, r, cats)
}

func (h Handler) CreateCategory(w http.ResponseWriter, r *http.Request) {
	category, err := h.manager.NewCategory(r.Body)
	if err != nil {
		log.Println("error creating category", err)
		http.Error(w, "error creating category", http.StatusInternalServerError)
		return
	}
	h.f.RenderJson(w, r, category)
}

func (h Handler) GetCategory(w http.ResponseWriter, r *http.Request) {
	slug := chi.URLParam(r, "slug")
	limit := h.queryInt(r, "limit", 20)
	page := h.queryInt(r, "page", 1)
	category, err := h.manager.GetCategoryWithCourses(slug, limit, (page-1)*limit)
	if err != nil {
		log.Println("error loading category", err)
		http.Error(w, "error loading category", http.StatusInternalServerError)
		return
	}
	h.f.RenderJson(w, r, category)
}

func (h Handler) UpdateCategory(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	cat, err := h.manager.UpdateCategory(id, r.Body)
	if err != nil {
		log.Println("error updating category", err)
		http.Error(w, "error updating category", http.StatusInternalServerError)
		return
	}
	h.f.RenderJson(w, r, cat)
}

func (h Handler) GetCourses(w http.ResponseWriter, r *http.Request) {
	limit := h.queryInt(r, "limit", 15)
	page := h.queryInt(r, "page", 1)
	courses, err := h.manager.GetCourses(limit, (page-1)*limit)
	if err != nil {
		log.Println("Error getting courses", err)
		http.Error(w, "error getting courses", http.StatusInternalServerError)
		return
	}
	h.f.RenderJson(w, r, courses)
}

func (h Handler) GetCourse(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	course, err := h.manager.GetCourse(id)
	if err != nil {
		log.Println("error getting course", err)
		http.Error(w, "error getting course", http.StatusInternalServerError)
		return
	}
	h.f.RenderJson(w, r, course)
}

func (h Handler) CreateCourse(w http.ResponseWriter, r *http.Request) {
	course, err := h.manager.NewCourse(r.Body)
	if err != nil {
		log.Println("Error creating course", err)
		http.Error(w, "error creating course", http.StatusInternalServerError)
		return
	}
	h.f.RenderJson(w, r, course)
}

func (h Handler) UpdateCourse(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	course, err := h.manager.UpdateCourse(id, r.Body)
	if err != nil {
		log.Println("error updating course", err)
		http.Error(w, "error updating course", http.StatusInternalServerError)
		return
	}
	h.f.RenderJson(w, r, course)
}

func (h Handler) CreateLesson(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	lesson, err := h.manager.NewLesson(id, r.Body)
	if err != nil {
		log.Println("Error storing lesson", err)
		http.Error(w, "error storing lesson", http.StatusInternalServerError)
		return
	}
	h.f.RenderJson(w, r, lesson)
}

func (h Handler) GetLesson(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "lid")
	lesson, err := h.manager.GetLesson(id)
	if err != nil {
		log.Println("error getting lesson", err)
		http.Error(w, "error getting lesson", http.StatusInternalServerError)
		return
	}
	h.f.RenderJson(w, r, lesson)
}

func (h Handler) UpdateLesson(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "lid")
	lesson, err := h.manager.UpdateLesson(id, r.Body)
	if err != nil {
		log.Println("error updating lesson", err)
		http.Error(w, "error updating lesson", http.StatusInternalServerError)
		return
	}
	h.f.RenderJson(w, r, lesson)
}

func (h Handler) DeleteLesson(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	lid := chi.URLParam(r, "lid")
	err := h.manager.DeleteLesson(id, lid)
	if err != nil {
		log.Println("error deleting lesson", err)
		http.Error(w, "error deleting lesson", http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "deleted lesson")
}

func (h Handler) CreateHandout(w http.ResponseWriter, r *http.Request) {
	lid := chi.URLParam(r, "lid")
	handout, err := h.manager.CreateHandout(lid, r.Body)
	if err != nil {
		log.Println("error creating handout", err)
		http.Error(w, "error creating handout", http.StatusInternalServerError)
		return
	}
	h.f.RenderJson(w, r, handout)
}

func (h Handler) DeleteHandout(w http.ResponseWriter, r *http.Request) {
	lid := chi.URLParam(r, "lid")
	hid := chi.URLParam(r, "hid")
	err := h.manager.DeleteHandout(lid, hid)
	if err != nil {
		log.Println("error deleting handout", err)
		http.Error(w, "error deleting handout", http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "deleted handout")
}

func (h Handler) queryInt(r *http.Request, key string, def int) int {
	val := def
	str := r.URL.Query().Get(key)
	if str != "" {
		if i, iErr := strconv.Atoi(str); iErr == nil {
			val = i
		}
	}
	return val
}
