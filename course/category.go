package course

import (
	"strings"
	"time"

	"github.com/gofrs/uuid"

	"github.com/jinzhu/gorm"
)

type Category struct {
	ID          string     `gorm:"type:varchar(48)" json:"id"`
	Title       string     `json:"title"`
	Slug        string     `gorm:"index:category_slug_idx" json:"slug"`
	Description string     `json:"description"`
	Cover       string     `gorm:"type:varchar(1024)" json:"cover"`
	Courses     []Course   `gorm:"foreignkey:CategoryID" json:"courses"`
	CreatedAt   time.Time  `json:"createdAt"`
	UpdatedAt   time.Time  `json:"updatedAt"`
	DeletedAt   *time.Time `sql:"index" json:",omitempty"`

	OgURL              string `json:"ogurl"`
	OgTitle            string `json:"ogtitle"`
	OgDescription      string `json:"ogdescription"`
	OgImage            string `json:"ogimage"`
	OgImageType        string `json:"ogimagetype"`
	OgImageWidth       string `json:"ogimagewidth"`
	OgImageHeight      string `json:"ogimageheight"`
	FbAppID            string `json:"fbappid"`
	OgType             string `json:"ogtype"`
	OgLocale           string `json:"oglocale"`
	TwitterCard        string `json:"twittercard"`
	TwitterSite        string `json:"twittersite"`
	TwitterTitle       string `json:"twittertitle"`
	TwitterDescription string `json:"twitterdescription"`
	TwitterCreator     string `json:"twittercreator"`
	TwitterImage       string `json:"twitterimage"`
}

// GetTitle returns the page title, intended to be called from the template with a default value
func (c Category) GetTitle(def string) string {
	if c.Title == "" {
		return def
	}
	return c.Title
}

// GetDescription returns page description, intended to be called from the template with a default value
func (c Category) GetDescription(def string) string {
	if c.Description == "" {
		return def
	}
	return c.Description
}

// GetOgUrl returns the open graph url or the canonical url
func (c Category) GetOgUrl(def string) string {
	if c.OgURL != "" {
		return c.OgURL
	}
	return def
}

// GetOgTitle returns the open graph title for the page, or the regular title, or the default value passed in
func (c Category) GetOgTitle(def string) string {
	if c.OgTitle != "" {
		return c.OgTitle
	} else if c.Title != "" {
		return c.Title
	} else {
		return def
	}
}

// GetOgDescription returns the open graph description, the regular description, or the default value passed in
func (c Category) GetOgDescription(def string) string {
	if c.OgDescription != "" {
		return c.OgDescription
	} else if c.Description != "" {
		return c.Description
	} else {
		return def
	}
}

// GetOgImage returns the open graph image or the default value passed in
func (c Category) GetOgImage(def string) string {
	if c.OgImage != "" {
		return c.OgImage
	}
	return def
}

// GetFbAppId returns the fb app id or the default value passed in
func (c Category) GetFbAppId(def string) string {
	if c.FbAppID != "" {
		return c.FbAppID
	}
	return def
}

// GetOgType returns the open graph type or the default website
func (c Category) GetOgType() string {
	if c.OgType != "" {
		return c.OgType
	}
	return "website"
}

// GetOgLocale returns the open graph locale or the default locale
func (c Category) GetOgLocale() string {
	if c.OgLocale != "" {
		return c.OgLocale
	}
	return "en_US"
}

// GetTwitterCard returns the twitter card summary
func (c Category) GetTwitterCard(def string) string {
	if c.TwitterCard != "" {
		return c.TwitterCard
	} else if c.Title != "" {
		return c.Title
	}
	return def
}

// GetTwitterSite returns the twitter site or default.  i.e. @KendellFab
func (c Category) GetTwitterSite(def string) string {
	if c.TwitterSite != "" {
		return c.TwitterSite
	}
	return def
}

// GetTwitterTitle returns the twitter title or the page title or the default
func (c Category) GetTwitterTitle(def string) string {
	if c.TwitterTitle != "" {
		return c.TwitterTitle
	} else if c.Title != "" {
		return c.Title
	}
	return def
}

// GetTwitterDescription returns the twitter description or the page description or the default
func (c Category) GetTwitterDescription(def string) string {
	if c.TwitterDescription != "" {
		return c.TwitterDescription
	} else if c.Description != "" {
		return c.Description
	}
	return def
}

// GetTwitterCreator returns the twitter creator or default
func (c Category) GetTwitterCreator(def string) string {
	if c.TwitterCreator != "" {
		return c.TwitterCreator
	}
	return def
}

// GetTwitterImage returns the twitter image or default
func (c Category) GetTwitterImage(def string) string {
	if c.TwitterImage != "" {
		return c.TwitterImage
	}
	return def
}

func (c *Category) BeforeCreate(scope *gorm.Scope) error {
	id, err := uuid.NewV4()
	if err != nil {
		return nil
	}
	return scope.SetColumn("ID", id.String())
}

func (c *Category) BeforeSave(scope *gorm.Scope) error {
	slug := strings.ToLower(strings.Replace(c.Title, " ", "-", -1))
	c.Slug = slug
	return nil
}
