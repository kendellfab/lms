package course

import (
	"time"

	"github.com/gofrs/uuid"
	"github.com/jinzhu/gorm"
)

type Handout struct {
	ID        string     `gorm:"type:varchar(48)" json:"id"`
	Name      string     `gorm:"type:varchar(1024)" json:"name"`
	Path      string     `gorm:"type:varchar(4096)" json:"path"`
	LessonID  string     `json:"lessonId"`
	CreatedAt time.Time  `json:"createdAt" json:"createdAt"`
	UpdatedAt time.Time  `json:"updatedAt" json:"updatedAt"`
	DeletedAt *time.Time `sql:"index" json:",omitempty"`
}

func (h *Handout) BeforeCreate(scope *gorm.Scope) error {
	id, err := uuid.NewV4()
	if err != nil {
		return nil
	}
	return scope.SetColumn("ID", id.String())
}
