module gitlab.com/kendellfab/lms

go 1.12

require (
	github.com/denisenkom/go-mssqldb v0.0.0-20191128021309-1d7a30a10f73 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/erikstmartin/go-testdb v0.0.0-20160219214506-8d10e4a1bae5 // indirect
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/gorilla/sessions v1.2.0
	github.com/jinzhu/gorm v1.9.2
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/jinzhu/now v1.1.1 // indirect
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/lib/pq v1.0.0 // indirect
	github.com/mattn/go-sqlite3 v2.0.1+incompatible // indirect
	github.com/russross/blackfriday/v2 v2.0.1 // indirect
	github.com/spf13/viper v1.3.2
	github.com/wader/gormstore v0.0.0-20190904144442-d36772af4310
	github.com/yuin/goldmark v1.1.12 // indirect
	gitlab.com/kendellfab/fazer v0.10.2
	gitlab.com/kendellfab/web v0.2.1
	golang.org/x/crypto v0.0.0-20190820162420-60c769a6c586
	golang.org/x/net v0.0.0-20191204025024-5ee1b9f4859a // indirect
)
